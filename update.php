<?php 
include('config.php');
if(isset($_REQUEST['id'])) {
	$id = $_REQUEST['id'];
	}
	else {
		header('location: edit.php');
	}
	
if(isset($_POST['form2'])){
	try{
		if(empty($_POST['st_name'])) {
			throw new Exception('Name can not be empty !');
		}
		if(empty($_POST['st_id'])) {
			throw new Exception ('Id can not be empty !');
		}
		if(empty($_POST['st_department'])) {
			throw new Exception('Department can not be empty !');
		}
		if(empty($_POST['st_semester'])) {
			throw new Exception('Semester can not be empty !');
		}
		if(empty($_POST['st_email'])) {
			throw new Exception('Email can not be empty !');
		}
		if(empty($_POST['st_phone'])) {
			throw new Exception('Phone Number can not be empty !');
		}
		if(empty($_POST['st_address'])) {
			throw new Exception('Address can not be empty !');
		}
		$statement = $db->prepare("update tbl_student set st_name=?, st_id=?, st_department=?, st_semester=?, st_email =?, st_phone =?, st_address= ? where s_id=?");
		$statement->execute(array($_POST['st_name'],$_POST['st_id'],$_POST['st_department'],$_POST['st_semester'], $_POST['st_email'], $_POST['st_phone'], $_POST['st_address'], $id));	
					
		
		
		header('location: edit.php');
	}
	catch(Exception $e) {
		$error_message = $e->getMessage();
	}
}


?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Student Information Management System</title>
	<link rel="stylesheet" href="style.css" />
	<style>
		form{margin: 10px 0px 0px 500px}
	</style>
</head>
<body style="background: url('img.jpg'); margin: 50px 0 auto;">
	<h2 style="text-align: center;">Update Information</h2>
	<?php  
		if(isset($error_message)) {echo "<div class='error'>".$error_message."</div>";}
		if(isset($success_message)) {echo "<div class='success'>".$success_message."</div>";}
	?>
	<br>
	<?php

		$statement = $db->prepare("select * from tbl_student where s_id=?");
		$statement->execute(array($id));
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $row)
		{
			$st_name_old = $row['st_name'];
			$st_id_old = $row['st_id'];
			$st_department_old = $row['st_department'];
			$st_semester_old = $row['st_semester'];
			$st_email_old = $row['st_email'];
			$st_phone_old = "0".$row['st_phone'];
			$st_address_old = $row['st_address'];
		}

	?>
	<form action="" method="post">
		<table>
			<tr>
				<td>Name :</td>
				<td><input type="text" name="st_name" value="<?php echo $st_name_old; ?>"/></td>
			</tr>
			<tr>
				<td>Id :</td>
				<td><input type="text" name="st_id" value="<?php echo $st_id_old; ?>"/></td>
			</tr>
			<tr>
				<td>Department :</td>
				<td><input type="text" name="st_department" value="<?php echo $st_department_old; ?>"/></td>
			</tr>
			<tr>
				<td>Semester :</td>
				<td><input type="text" name="st_semester" value="<?php echo $st_semester_old; ?>"/></td>
			</tr>
			
			<tr>
				<td>Email :</td>
				<td><input type="email" name="st_email" value="<?php echo $st_email_old; ?>"/></td>
			</tr>
			<tr>
				<td>Phone Number :</td>
				<td><input type="text" name="st_phone" maxlength="11" value="<?php echo $st_phone_old; ?>"/></td>
			</tr>
			<tr>
				<td>Address :</td>
				<td class="address"><input type="text" name="st_address"  value="<?php echo $st_address_old; ?>"/><td>
				
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="form2" value="Update"/></td>
			</tr>
		</table>
	</form>
	<p><a href="edit.php">Back </a></p>
</body>
</html>