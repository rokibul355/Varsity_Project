<?php 
include('config.php');

if(isset($_REQUEST['id'])) {
	$id = $_REQUEST['id'];
	try {
		$statement = $db->prepare("delete from tbl_student where s_id=?");
		$statement->execute(array($id));
		$success_message = "Data has been deleted successfully.";
	}
	catch(Exception $e) {
		$error_message = $e->getMessage;
	}
}


?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Student Information Management System</title>
	<link rel="stylesheet" href="style.css" />
	<script>
		function confirm_delete() {
			return confirm('Are you sure want to delete this data?');
		}
	</script>
	<style>
		form{margin: 10px 0px 0px 500px}
	</style>
</head>
<body style="background: url('img.jpg'); margin: 30px 0 auto;">
	<h2 style="text-align: center; margin-bottom: -40px;">Delete Information</h2>
	<div class="search">
	<form action="search_delete.php" method="post">
	<table>
	<tr>
		<td><input type="text" name="st_search"  class="search"/></td>
		<td><input type="submit" value="search"/></td>
	</tr>
	</table>
	</form>
	</div>
	<br>
	<?php if(isset($success_message)){echo "<div class='success'>". $success_message."</div>"; } ?>
	<br>
	<div class="container">
		<table class="tbl2" border="1" cellspacing="0" cellpadding="5" width="100%">
			<tr>
				<th width="10%">No</th>
				<th width="40%">Name</th>
				<th width="40%">Id</th>
				<th width="10%">Action</th>
				
			</tr>
			<?php
			$i=0;
	
			$statement = $db->prepare("select * from tbl_student");
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			foreach($result as $row)
			{
				$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row['st_name']; ?></td>
					<td><?php echo $row['st_id']; ?></td>
					<td><a onclick="return confirm_delete();" href="delete.php?id=<?php echo $row['s_id']; ?>"><button type="button" class="btn">Delete</button></a></td>
					
				</tr>
				<?php
			}
			?>
			
			
		</table>
	</div>
	<p><a href="index.php">Back to main page. </a></p>
</body>
</html>