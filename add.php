<?php 
include('config.php');
if(isset($_POST['form1'])){
	try{
		if(empty($_POST['st_name'])) {
			throw new Exception('Name can not be empty !');
		}
		if(empty($_POST['st_id'])) {
			throw new Exception ('Id can not be empty !');
		}
		if(empty($_POST['st_department'])) {
			throw new Exception('Department can not be empty !');
		}
		if(empty($_POST['st_semester'])) {
			throw new Exception('Semester can not be empty !');
		}
		if(empty($_POST['st_email'])) {
			throw new Exception('Email can not be empty !');
		}
		if(empty($_POST['st_phone'])) {
			throw new Exception('Phone Number can not be empty !');
		}
		if(empty($_POST['st_address'])) {
			throw new Exception('Address can not be empty !');
		}
		$statement = $db->prepare("insert into tbl_student (st_name, st_id, st_department, st_semester, st_email, st_phone, st_address) values (?,?,?,?,?,?,?)");
		$statement->execute(array($_POST['st_name'], $_POST['st_id'], $_POST['st_department'], $_POST['st_semester'], $_POST['st_email'], $_POST['st_phone'],$_POST['st_address']));
		
		$success_message = 'Data has been inserted successfully .';
	}
	catch(Exception $e) {
		$error_message = $e->getMessage();
	}
}


?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<meta charset="UTF-8">
	<title>Student Information Management System</title>
	<link rel="stylesheet" href="style.css" />
	<style>
		form{margin: 10px 0px 0px 500px}
	</style>
</head>
<body style="background: url('img.jpg'); margin: 50px 0 auto;">
	<h2 style="text-align: center;">Add Information</h2>
	<?php  
		if(isset($error_message)) {echo "<div class='error'>".$error_message."</div>";}
		if(isset($success_message)) {echo "<div class='success'>".$success_message."</div>";}
	?>
	<br>
	<form action="" method="post">
		<table>
			<tr>
				<td>Name :</td>
				<td><input type="text" name="st_name"/></td>
			</tr>
			<tr>
				<td>Id :</td>
				<td><input type="text" name="st_id"/></td>
			</tr>
			<tr>
				<td>Department :</td>
				<td><input type="text" name="st_department"/></td>
			</tr>
			<tr>
				<td>Semester :</td>
				<td><input type="text" name="st_semester"/></td>
			</tr>
			
			<tr>
				<td>Email :</td>
				<td><input type="email" name="st_email" /></td>
			</tr>
			<tr>
				<td>Phone Number :</td>
				<td><input type="tel" name="st_phone" value="" maxlength="11" pattern=".{11,11}"/></td>
			</tr>
			<tr>
				<td>Address :</td>
				<td><textarea name="st_address" id="" cols="40" rows="3"></textarea><td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="form1" value="Save"/></td>
			</tr>
		</table>
	</form>
	<p><a href="index.php">Back to main page. </a></p>
</body>
</html>